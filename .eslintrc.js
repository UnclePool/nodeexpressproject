module.exports = {
  env: {
    browser: true,
    node: true,
    commonjs: true,
    es2020: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 11,
  },
  rules: {
    semi: 'error',
    'no-console': 'off',
    'prefer-template': 'off',
    'no-path-concat': 'off',
    'no-restricted-syntax': 'off',
    'guard-for-in': 'off',
    'no-underscore-dangle': 'off',
    'dot-notation': 'off',
  },
};
