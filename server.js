const express = require('express');
const fetch = require('node-fetch');
const bodyParser = require('body-parser');
const Formidable = require('formidable');
const fortune = require('./lib/fortune');
const weather = require('./lib/weather');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));

app.set('views', './views');
app.set('view engine', 'pug');
app.set('port', process.env.PORT || 3000);

app.use((req, res, next) => {
  if (!res.locals.partials) res.locals.partials = {};
  res.locals.partials.weatherContext = weather.getWeatherData();
  next();
});

app.get('/', (req, res) => {
  res.render('home', { msg: 'Welcome to home page' });
});

app.get('/about', (req, res) => {
  res.render('about', { msg: 'Welcome to about page', fortune: fortune.getFortune() });
});

app.get('/headers', (req, res) => {
  res.set('Content-Type', 'text/plain');
  let s = '';
  for (const key in req.headers) {
    s += `${key} : ${req.headers[key]}///`;
  }
  res.send(s);
});

app.get('/newsletter', (req, res) => {
  res.render('newsletter', { csrf: 'CSRF token goes here' });
});

app.post('/process', (req, res) => {
  console.log(`Form (from querystring) - ${req.query.form}`);
  console.log(`CSRF token - ${req.body.csrf}`);
  console.log(`Name - ${req.body.name}`);
  console.log(`Email - ${req.body.email}`);
  res.redirect(303, '/');
});

app.get('/contest/vacation-photo', (req, res) => {
  res.render('contest/vacation-photo', {
    year: new Date().getFullYear(),
    month: new Date().getMonth() + 1,
  });
});

app.post('/contest/vacation-photo/:month/:year', (req, res) => {
  // const form = new Formidable({ multiples: true });
  // form.parse(req, (err, fields, files) => {
  //   if (err) {
  //     // eslint-disable-next-line no-undef
  //     next(err);
  //     return;
  //   }
  //   res.json({ fields, files });
  // });
  const form = new Formidable.IncomingForm();
  form.parse(req, (err, fields, files) => {
    if (err) res.redirect(303, 'ERROR PARSE');
    console.log('fields');
    console.log(fields);
    console.log('files');
    console.log(files);
    res.send('success');
  });
});

app.use(express.static(__dirname + '/public'));

app.use((req, res) => {
  res.status(404);
  res.render('404', { msg: '404 - Not found' });
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  console.log(err.stack);
  res.status(500);
  res.render('500', { msg: '500 - Internal Error' });
});

app.listen(app.get('port'), () => {
  console.log('Server is start at http://localhost:' + app.get('port'));
});
